import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators } from '@angular/forms'
import { UserService } from '../service/user.service';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {
  registerForm:FormGroup
  constructor(private fb: FormBuilder,private user:UserService) { }

  ngOnInit() {
    this.registerForm=this.fb.group({
      email:[''],
      firstname:[''],
      secondname:[''],
      location:[''],
      password:[''],
      confirmPassword:[''],
      type:[]
    })
  }

  get password(){
    return this.registerForm.get('password')
  }
  get confirmPassword(){
    return this.registerForm.get('confirmPassword')
  }

  register(){

    if(this.password.value==this.confirmPassword.value){
      this.user.register(this.registerForm.value);
    }
    else{
      Swal.fire({
        title: 'Error!',
        text: 'Password Not Matched',
        icon: 'warning',
        confirmButtonText: 'OK'
      })
    }
  }
}
