import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import Swal from 'sweetalert2'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url="http://34.207.191.60:9898"
  // url="http://localhost:3000"
  constructor(private http:HttpClient,private router:Router) { }



  //////set Jwt
  setJwt(token)
  {
     if(token)
       localStorage.setItem('society-token',token);
  }
  getJwt()
  {
     let token = localStorage.getItem('society-token');
     if(token)
     {
       let payload = localStorage.getItem('society-token');
       payload = window.atob(payload);
       return JSON.parse(payload);
     }
     else
       return null
  }
  removeJwt()
  {
   localStorage.removeItem('society-token')
  }
  ////////////////register Society
  register(obj){
    console.log(obj)
    if(obj.type=="0"){
       this.http.post(`${this.url}/society/registerSociety`,obj).subscribe((res:any)=>{
        if(res.success){
          Swal.fire({
            title: 'Registered',
            text: `${res.message}`,
            icon: 'success',
            confirmButtonText: 'OK'
          })
        }
        else{
          Swal.fire({
            title: 'Error!',
            text: `${res.message}`,
            icon: 'warning',
            confirmButtonText: 'OK'
          })
        }
      })
    }
    
    if(obj.type=="1"){
      
      this.http.post(`${this.url}/corporate/registerCorporate`,obj).subscribe((res:any)=>{
        if(res.success){
          Swal.fire({
            title: 'Registered',
            text: `${res.message}`,
            icon: 'success',
            confirmButtonText: 'OK'
          })
        }
        else{
          Swal.fire({
            title: 'Error!',
            text: `${res.message}`,
            icon: 'warning',
            confirmButtonText: 'OK'
          })
        }
      })
    }
    
  }


  ////////////////////////////login
  login(obj){
    if(obj.type=="0"){
      this.http.post(`${this.url}/society/loginSociety`,obj).subscribe((res:any)=>{
        if(res.success){
          Swal.fire({
            title: 'Login',
            text: `${res.message}`,
            icon: 'success',
            confirmButtonText: 'OK'
          })
          this.setJwt(res.data);

  
        }
        else{
          Swal.fire({
            title: 'Error!',
            text: `${res.message}`,
            icon: 'warning',
            confirmButtonText: 'OK'
          })
        }
      })
    }
    
    if(obj.type=="1"){
      this.http.post(`${this.url}/corporate/loginCorporate`,obj).subscribe((res:any)=>{
        if(res.success){
          Swal.fire({
            title: 'Registered',
            text: `${res.message}`,
            icon: 'success',
            confirmButtonText: 'OK'
          })
          this.setJwt(res.data)
        }
        else{
          Swal.fire({
            title: 'Error!',
            text: `${res.message}`,
            icon: 'warning',
            confirmButtonText: 'OK'
          })
        }
      })
    }
    

  }
}
