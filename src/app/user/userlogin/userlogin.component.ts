import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators } from '@angular/forms'
import { UserService } from '../service/user.service';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.css']
})
export class UserloginComponent implements OnInit {
show=1;
loginForm:FormGroup
  constructor(private fb:FormBuilder,private user:UserService) { }

  ngOnInit() {
    this.show=1;
    this.loginForm=this.fb.group({
      email:[''],
      password:[''],
      type:[''],
    })
  }
  showPage(show){
    this.show=show;
    console.log(show)
  }
  login(){
    this.user.login(this.loginForm.value)
  }

}
